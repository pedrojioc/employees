@extends('layouts.application')
@section('content')
  <div class="row mt-4">
    <div class="col-12">
      <div class="jumbotron">
        <h1 class="display-4">Hello, Nuvola!</h1>
        <p class="lead">Esta es una aplicación sencilla que realiza las operaciones básicas CRUD. Consta de tres secciones; Áreas, Cargos y Empleados. Este último depende de un cargo y un área, y estos pueden ser administrados en su correspondiente sección.</p>
        <hr class="my-4">
        <p>Pedro José Jiménez Ochoa.</p>
      
      </div>
    </div>
  </div>
@endsection