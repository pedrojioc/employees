@extends('layouts.application')
@section('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <style>
    .error {
      border-color: red;
    }
  </style>
@endsection
@section('content')
  <div class="row mt-4 justify-content-center">
    <div class="col-sm-12 col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Agregar nuevo empleado</h5>
        </div>
        <div class="card-body">
          <form id="form_add_employee" name="form_add_employee" action="{{ route('employee.create') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="">Nombres</label>
              <input name="first_name" id="first_name" type="text" class="form-control" placeholder="Jhon doe" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Apellidos</label>
              <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Smith" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Identificación</label>
              <input name="employee_id" id="employee_id" type="text" class="form-control" placeholder="1.059.000.000" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Fecha de nacimiento</label>
              <input name="birthdate" id="birthdate" type="text" class="form-control" placeholder="Fecha de nacimiento" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Dirección</label>
              <input name="address" id="address" type="text" class="form-control" placeholder="Cra 89 #46 - 40" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Teléfono</label>
              <input name="telephone" id="telephone" type="text" class="form-control" placeholder="Número de teléfono" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Salario</label>
              <input name="salary" id="salary" type="text" class="form-control" placeholder="Salario" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Area</label>
              <select name="area_id" id="area_id" class="form-control">
                @foreach ($areas as $area)
                  <option value="{{ $area->id }}">{{ $area->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">Cargo</label>
              <select name="position_id" id="position_id" class="form-control">
                @foreach ($positions as $position)
                  <option value="{{ $position->id }}">{{ $position->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">Fecha de admisión</label>
              <input name="date_of_admission" id="date_of_admission" type="text" class="form-control" placeholder="Fecha de ingreso" maxlength="100">
            </div>

            <div class="row">
              <div class="col d-flex flex-row">
                <a href="/employees" class="btn btn-secondary">Volver</a>
              </div>
              <div class="col d-flex flex-row-reverse">
                <button type="button" class="btn btn-primary ml-2" id="btn_add">Guardar</button>
                <button type="button" class="btn btn-danger" id="btn_cancel">Cancelar</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('custom-js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript">
  const FORM = document.getElementById('form_add_employee')
  const BTN_ADD = document.getElementById('btn_add')
  const BTN_CANCEL = document.getElementById('btn_cancel')
  const FIELD_NAME = document.getElementById('name')
  const FIELD_DESCRIPTION = document.getElementById('description')
  class Employee {
    constructor() {
      this.addEvents()
      this.initialize()
      console.log('init')
    }

    initialize = () => {
      $('#birthdate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      $('#date_of_admission').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
    }

    addEvents = () => {
      BTN_ADD.addEventListener('click', this.createEmployee)
      BTN_CANCEL.addEventListener('click', this.handleCancelClick)
    }

    createEmployee = (event) => {
      let valid = this.validate()
      if(!valid) return
      FORM.submit()
    }

    handleCancelClick = () => {
      for(let i = 0; i < FORM.length; i++) {
        if (FORM[i].type == 'text') {
          FORM[i].value = ''
        }
      }
    }

    validate = () => {
      let valid = true
      for(let i = 0; i < FORM.length; i++) {
        if (FORM[i].type == 'text' || FORM[i].type == 'select-one') {
          if (FORM[i].value.length === 0) {
            valid = false
           FORM[i].classList.add('error') 
          } else {
            FORM[i].classList.remove('error') 
          }
        }
      }
      return valid
     
    }
  }

  var employee = new Employee
  </script>
@endsection