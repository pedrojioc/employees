@extends('layouts.application')
@section('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <style>
    .error {
      border-color: red;
    }
  </style>
@endsection
@section('content')
  <div class="row mt-4 justify-content-center">
    <div class="col-sm-12 col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Agregar nuevo empleado</h5>
        </div>
        <div class="card-body">
          <form id="form_edit_employee" name="form_edit_employee" action="{{ route('employee.update') }}" method="POST" enctype="multipart/form-data">
            @csrf
          <input type="hidden" name="id" id="id" value="{{$employee->id}}">
            <div class="form-group">
              <label for="">Nombres</label>
            <input name="first_name" id="first_name" type="text" class="form-control" placeholder="Jhon doe" maxlength="50" value="{{$employee->first_name}}">
            </div>
            <div class="form-group">
              <label for="">Apellidos</label>
              <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Smith" maxlength="50" value="{{$employee->last_name}}">
            </div>
            <div class="form-group">
              <label for="">Identificación</label>
              <input name="employee_id" id="employee_id" type="text" class="form-control" placeholder="1.059.000.000" maxlength="50" value="{{$employee->employee_id}}">
            </div>
            <div class="form-group">
              <label for="">Fecha de nacimiento</label>
              <input name="birthdate" id="birthdate" type="text" class="form-control" placeholder="Fecha de nacimiento" maxlength="50" value="{{$employee->birthdate}}">
            </div>
            <div class="form-group">
              <label for="">Dirección</label>
              <input name="address" id="address" type="text" class="form-control" placeholder="Cra 89 #46 - 40" maxlength="50" value="{{$employee->address}}">
            </div>
            <div class="form-group">
              <label for="">Teléfono</label>
              <input name="telephone" id="telephone" type="text" class="form-control" placeholder="Número de teléfono" maxlength="50" value="{{$employee->telephone}}">
            </div>
            <div class="form-group">
              <label for="">Salario</label>
              <input name="salary" id="salary" type="text" class="form-control" placeholder="Salario" maxlength="50" value="{{$employee->salary}}">
            </div>
            <div class="form-group">
              <label for="">Area</label>
              <select name="area_id" id="area_id" class="form-control">
                @foreach ($areas as $area)
                  @if($area->id == $employee->area_id)
                    <option value="{{ $area->id }}" selected>{{ $area->name }}</option>
                  @else
                    <option value="{{ $area->id }}">{{ $area->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">Cargo</label>
              <select name="position_id" id="position_id" class="form-control">
                @foreach ($positions as $position)
                  @if($position->id == $employee->position_id)
                    <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                  @else
                    <option value="{{ $position->id }}">{{ $position->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">Fecha de admisión</label>
              <input name="date_of_admission" id="date_of_admission" type="text" class="form-control" placeholder="Fecha de ingreso" maxlength="100" value="{{$employee->date_of_admission}}">
            </div>

            <div class="row">
              <div class="col d-flex flex-row">
                <a href="/employees" class="btn btn-secondary">Volver</a>
              </div>
              <div class="col d-flex flex-row-reverse">
                <button type="button" class="btn btn-primary ml-2" id="btn_add">Guardar</button>
                <button type="button" class="btn btn-danger">Cancelar</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('custom-js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript">
  const FORM = document.getElementById('form_edit_employee')
  const BTN_ADD = document.getElementById('btn_add')
  const FIELD_NAME = document.getElementById('name')
  const FIELD_DESCRIPTION = document.getElementById('description')
  class Employee {
    constructor() {
      this.addEventCreate()
      this.initialize()
      console.log('init')
    }

    initialize = () => {
      $('#birthdate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      $('#date_of_admission').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
    }

    addEventCreate = () => {
      BTN_ADD.addEventListener('click', this.createEmployee)
    }

    createEmployee = (event) => {
      let valid = this.validate()
      if(!valid) return
      FORM.submit()
    }

    validate = () => {
      let valid = true
      for(let i = 0; i < FORM.length; i++) {
        if (FORM[i].type == 'text') {
          if (FORM[i].value.length === 0) {
            valid = false
           FORM[i].classList.add('error') 
          } else {
            FORM[i].classList.remove('error') 
          }
        }
      }
      return valid
      // if(FIELD_NAME.value.length === 0) {
      //   FIELD_NAME.classList.add('error')
      //   return false
      // } else {
      //   FIELD_NAME.classList.remove('error')
      // }
      // if(FIELD_DESCRIPTION.value.length === 0) {
      //   FIELD_DESCRIPTION.classList.add('error')
      //   return false
      // } else {
      //   FIELD_DESCRIPTION.classList.remove('error')
      // }
      // return true
    }
  }

  var employee = new Employee
  </script>
@endsection