@extends('layouts.application')
@section('custom-css')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
  <div class="row mt-4">
    <div class="col d-flex flex-row-reverse">
      <a class="btn btn-success " href="employees/new">Agregar</a>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col-12">
      <table id="area_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th># Identificación</th>
            
            
            <th>Teléfono</th>
            <th>Salario</th>
            <th>Area</th>
            <th>Cargo</th>
            
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employees as $employee)
            <tr>
              <td>{{ $employee->id }}</td>
              <td>{{ $employee->first_name." ".$employee->last_name }}</td>
              <td>{{ $employee->employee_id }}</td>
              <td>{{ $employee->telephone }}</td>
              <td>{{ $employee->salary }}</td>
              <td></td>
              <td>{{ $employee->position->name }}</td>
              
              
              <td style="display:flex;justify-content:center;">
                <div class="btn-group" role="group" aria-label="Basic example">
                  <a class="btn btn-light" href="/employees/show/{{ $employee->id }}" data-toggle="tooltip" data-placement="top" title="Ver"><i class="far fa-eye"></i></a>
                  <a class="btn btn-light" href="/employees/edit/{{ $employee->id }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                  <button type="button" class="btn btn-light" onclick="deleteEmployee({{$employee->id}})" data-toggle="tooltip" data-placement="top" title="Elimnar"><i class="fas fa-trash"></i></button>
                </div>
                
              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>

  <!-- Begin: modal -->
  <!-- 
  <div class="modal fade" id="my_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Nombre</label>
            <input name="name" id="name" type="text" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Descripción</label>
            <input name="description" id="description" type="text" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  -->
  <!-- End:modal -->
@endsection
@section('custom-js')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">
  $(document).ready( function () {
    $('#area_table').DataTable({
        "scrollX": true,
        "scrollY": true,
        scrollY: 500
    });
  } );
  
  function deleteEmployee(id) {
    
    // console.log('ejecutando request')
    // try {
    //   let response = await fetch(url, {
    //     method: 'get',
    //     headers: {
    //       'Accept': 'application/text',
    //       'Content-Type': 'application/text',
    //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    //   })
    //   console.log('resolve')
    //   let responseText = await response.text()
    //   return responseText
    // } catch (e) {
    //   console.error('Ha ocurrido un error: '+e)
    //   return
    // }

    let r = confirm("El registro será eliminada");
    if (r == true) {
      $.ajax({
        type:'GET',
        dataType:'json',
        url:'/employees/delete/'+id,
        
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      }).done(function(data) {
        let rs = parseInt(data)
          if(rs){
            location.reload();
          }else{
            alert('Ha ocurrido un error')
            return  false;
          }
      }).fail(function () {
        console.log('error')
      })
      
    }    
      
  }
  
  </script>
@endsection