@extends('layouts.application')
@section('content')
    

<div class="row justify-content-center">
  <div class="col-8">
    <div class="card">
      <h5 class="card-header">Información</h5>
      <div class="card-body">
        <h5 class="card-title">Datos del empleado</h5>
        
        <div class="form-group">
          <label for="">Nombres</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->first_name }}">
        </div>
        
        <div class="form-group">
          <label for="">Apellidos</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->last_name }}">
        </div>
        
        <div class="form-group">
          <label for="">Fecha de nacimiento</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->birthdate }}">
        </div>
        
        <div class="form-group">
          <label for="">Dirección</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->address }}">
        </div>
        
        <div class="form-group">
          <label for="">Teléfono</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->telephone }}">
        </div>
        
        <div class="form-group">
          <label for="">Salario</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->salary }}">
        </div>
        
        <div class="form-group">
          <label for="">Área</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->area->name }}">
        </div>
        
        <div class="form-group">
          <label for="">Cargo</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->position->name }}">
        </div>
        
        <div class="form-group">
          <label for="">Fecha de ingreso</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $employee->date_of_admission }}">
        </div>
        <a href="/employees" class="btn btn-secondary mt-4">Regresar</a>
      </div>
    </div>
  </div>
</div>
<br><br>
@endsection