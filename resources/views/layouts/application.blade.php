<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nuvola Test</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script src="https://kit.fontawesome.com/3e085ddea8.js"></script>
  <!-- Default Styles -->
  @include('extends.default-css')
  @yield('custom-css')
</head>
<body>
  <!-- Begin: header -->
  @include('extends.header')
  <!-- End: header -->
  
  <div class="container">
    <div class="row mt-4">
      <div class="col">
        @if (session('success'))
        <div class="alert alert-success alert-dismissible mb-2" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          {{ session('success') }}
        </div>
        @endif

        @if (session('notice'))
        <div class="alert alert-danger alert-dismissible mb-2" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          {{ session('notice') }}
        </div>
        @endif

      </div>
    </div>
  </div>
  <div id="wrapper" class="container">
    @yield('content')
  </div>


  <!-- Default JS -->
  @include('extends.default-js')
  @yield('custom-js')

</body>
</html>