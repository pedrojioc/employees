<header>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/">|Nuvola|</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link {{ Request::is( '/') ? 'active' : '' }}" href="/">Inicio</a>
        <a class="nav-item nav-link {{ Request::is( 'areas') ? 'active' : '' }}" href="/areas">Areas</a>
        <a class="nav-item nav-link {{ Request::is( 'positions') ? 'active' : '' }}" href="/positions">Cargos</a>
        <a class="nav-item nav-link {{ Request::is( 'employees') ? 'active' : '' }}" href="/employees">Empleados</a>
      </div>
    </div>
  </nav>
</header>