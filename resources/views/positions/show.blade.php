@extends('layouts.application')
@section('content')
    

<div class="row justify-content-center">
  <div class="col-8">
    <div class="card">
      <h5 class="card-header">Información</h5>
      <div class="card-body">
        <h5 class="card-title">Datos del cargo</h5>
        
        <div class="form-group">
          <label for="">Nombre</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $position->name }}">
        </div>
        
        <div class="form-group">
          <label for="">Descripción</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $position->description }}">
        </div>
        <a href="/positions" class="btn btn-light mt-4">Regresar</a>
      </div>
    </div>
  </div>
</div>

@endsection