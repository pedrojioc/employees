@extends('layouts.application')
@section('custom-css')
  <style>
    .error {
      border-color: red;
    }
  </style>
@endsection
@section('content')
  <div class="row mt-4 justify-content-center">
    <div class="col-sm-12 col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Agregar nuevo cargo</h5>
        </div>
        <div class="card-body">
          <form id="form_add_position" name="form_add_position" action="{{ route('position.create') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="">Nombre</label>
              <input name="name" id="name" type="text" class="form-control" placeholder="Nombre del cargo" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Descripción</label>
              <input name="description" id="description" type="text" class="form-control" placeholder="Descripción corta" maxlength="100">
            </div>

            <div class="row">
              <div class="col d-flex flex-row">
                <a href="/positions" class="btn btn-secondary">Volver</a>
              </div>
              <div class="col d-flex flex-row-reverse">
                <button type="button" class="btn btn-primary ml-2" id="btn_add">Guardar</button>
                <button type="button" class="btn btn-danger" id="btn_cancel">Cancelar</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('custom-js')
  <script type="text/javascript">
  const FORM = document.getElementById('form_add_position')
  const BTN_ADD = document.getElementById('btn_add')
  const BTN_CANCEL = document.getElementById('btn_cancel')
  
  class Position {
    constructor() {
      this.addEvents()
      console.log('init')
    }

    addEvents = () => {
      BTN_ADD.addEventListener('click', this.createPosition)
      BTN_CANCEL.addEventListener('click', this.handleCancelClick)
    }

    createPosition = (event) => {
      let valid = this.validate()
      if(!valid) return
      FORM.submit()
    }

    handleCancelClick = () => {
      for(let i = 0; i < FORM.length; i++) {
        
        if (FORM[i].type == 'text') {
          FORM[i].value = ''
        }
      }
    }

    validate = () => {
      let valid = true
      for(let i = 0; i < FORM.length; i++) {
        if (FORM[i].type == 'text' || FORM[i].type == 'select-one') {
          if (FORM[i].value.length === 0) {
            valid = false
           FORM[i].classList.add('error') 
          } else {
            FORM[i].classList.remove('error') 
          }
        }
      }
      return valid
    }
  }

  var position = new Position
  </script>
@endsection