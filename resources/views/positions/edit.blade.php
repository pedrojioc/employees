@extends('layouts.application')
@section('custom-css')
  <style>
    .error {
      border-color: red;
    }
  </style>
@endsection
@section('content')
  <div class="row mt-4 justify-content-center">
    <div class="col-sm-12 col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Editar cargo</h5>
        </div>
        <div class="card-body">
          <form id="form_edit_position" name="form_edit_position" action="{{ route('position.update') }}" method="POST" enctype="multipart/form-data">
            @csrf
          <input type="hidden" name="id" value="{{ $position->id }}">
            <div class="form-group">
              <label for="">Nombre</label>
              <input name="name" id="name" type="text" class="form-control" placeholder="Nombre del cargo" value="{{ $position->name }}" maxlength="50">
            </div>
            <div class="form-group">
              <label for="">Descripción</label>
              <input name="description" id="description" type="text" class="form-control" placeholder="Descripción corta" value="{{ $position->description }}" maxlength="100">
            </div>

            <div class="row">
              <div class="col d-flex flex-row">
                <a href="/positions" class="btn btn-secondary">Volver</a>
              </div>
              <div class="col d-flex flex-row-reverse">
                <button type="button" class="btn btn-primary ml-2" id="btn_add">Guardar</button>
                <button type="button" class="btn btn-danger">Cancelar</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('custom-js')
  <script type="text/javascript">
  const FORM = document.getElementById('form_edit_position')
  const BTN_ADD = document.getElementById('btn_add')
  const FIELD_NAME = document.getElementById('name')
  const FIELD_DESCRIPTION = document.getElementById('description')
  class Area {
    constructor() {
      this.addEventCreate()
      console.log('init')
    }

    addEventCreate = () => {
      BTN_ADD.addEventListener('click', this.createArea)
    }

    createArea = (event) => {
      let valid = this.validate()
      if(!valid) return
      FORM.submit()
    }

    validate = () => {
      
      console.log(FIELD_NAME.value.length)
      if(FIELD_NAME.value.length === 0) {
        FIELD_NAME.classList.add('error')
        return false
      } else {
        FIELD_NAME.classList.remove('error')
      }
      if(FIELD_DESCRIPTION.value.length === 0) {
        FIELD_DESCRIPTION.classList.add('error')
        return false
      } else {
        FIELD_DESCRIPTION.classList.remove('error')
      }
      return true
    }
  }

  var area = new Area
  </script>
@endsection