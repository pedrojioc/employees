@extends('layouts.application')
@section('custom-css')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
  
  <div class="row mt-4">
    <div class="col d-flex flex-row-reverse">
      <a class="btn btn-success " href="areas/new">Agregar</a>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col-12">
      <table id="area_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($areas as $area)
            <tr>
              <td>{{ $area->id }}</td>
              <td>{{ $area->name }}</td>
              <td>{{ $area->description }}</td>
              <td style="display:flex;justify-content:center;">
                <div class="btn-group" role="group" aria-label="Basic example">
                  <a class="btn btn-light" href="areas/show/{{ $area->id }}" data-toggle="tooltip" data-placement="top" title="Ver"><i class="far fa-eye"></i></a>
                  <a class="btn btn-light" href="areas/edit/{{ $area->id }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                  <button type="button" class="btn btn-light" onclick="deleteArea({{$area->id}})" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></button>
                </div>
              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>

  <!-- Begin: modal -->
  <!-- 
  <div class="modal fade" id="my_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Nombre</label>
            <input name="name" id="name" type="text" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Descripción</label>
            <input name="description" id="description" type="text" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  -->
  <!-- End:modal -->
@endsection
@section('custom-js')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  
  <script type="text/javascript">
  $(document).ready( function () {
    $('#area_table').DataTable({
      "scrollX": true,
        "scrollY": true,
        scrollY: 500
    });
  } );
  
  //const API_BASE = 'http://localhost/'

  // async function handleDelete(id) {
  //   let rs = await deleteArea(id)
  //   console.log(rs)
  // }


  function deleteArea(id) {
    
    // console.log('ejecutando request')
    // try {
    //   let response = await fetch(url, {
    //     method: 'get',
    //     headers: {
    //       'Accept': 'application/text',
    //       'Content-Type': 'application/text',
    //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    //   })
    //   console.log('resolve')
    //   let responseText = await response.text()
    //   return responseText
    // } catch (e) {
    //   console.error('Ha ocurrido un error: '+e)
    //   return
    // }

    let r = confirm("El área será eliminada");
    if (r == true) {
      $.ajax({
        type:'GET',
        dataType:'json',
        url:'/areas/delete/'+id,
        
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      }).done(function(data) {
        let rs = parseInt(data)
          if(rs === 100){
            alert('Entrada invalida')
            
          }else if(rs === 101){
            alert('Hay registros dependientes, operación invalida')
            return  false;
          }else{
            location.reload();
          }ss
      }).fail(function () {
        console.log('error')
      })
      
    }    
      
  }

  </script>
@endsection