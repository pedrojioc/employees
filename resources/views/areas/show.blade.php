@extends('layouts.application')
@section('content')
    

<div class="row justify-content-center">
  <div class="col-8">
    <div class="card">
      <h5 class="card-header">Información</h5>
      <div class="card-body">
        <h5 class="card-title">Datos de área</h5>
        
        <div class="form-group">
          <label for="">Nombre</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $area->name }}">
        </div>
        
        <div class="form-group">
          <label for="">Descripción</label>
          <input type="text" name="" id="" disabled="disabled" class="form-control" value="{{ $area->description }}">
        </div>
        <a href="/areas" class="btn btn-light mt-4">Regresar</a>
      </div>
    </div>
  </div>
</div>

@endsection