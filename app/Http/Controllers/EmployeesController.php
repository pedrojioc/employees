<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreEmployee;
use App\Employee;
use App\Area;
use App\Position;
class EmployeesController extends Controller
{
    public function index() {
        $employees = Employee::all();
        
        return view('employees.index', ['employees' => $employees]);
    }

    public function new() {
        $areas = Area::all();
        $positions = Position::all();

        return view('employees.new', ['areas' => $areas, 'positions' => $positions]);
    }

    public function create(StoreEmployee $request) {
        
        if(Employee::where('employee_id', $request->employee_id)->count() > 0){
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        $my_employee = new Employee();
        $my_employee->employee_id = $request->employee_id;
        $my_employee->first_name = $request->first_name;
        $my_employee->last_name = $request->last_name;
        $my_employee->birthdate = $request->birthdate;
        $my_employee->address = $request->address;
        $my_employee->telephone = $request->telephone;
        $my_employee->salary = $request->salary;
        $my_employee->area_id = $request->area_id;
        $my_employee->position_id = $request->position_id;
        $my_employee->date_of_admission = $request->date_of_admission;
        $rs = $my_employee->save();
   
        if($rs){
            return redirect('/employees/new')->with('success', 'Empleado agregado correctamente');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        

    }
    public function edit(Request $request) {
        $id = $request->id;

        $my_employee = Employee::find($id);
        
        if($my_employee == NULL){
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        $areas = Area::all();
        $positions = Position::all();

        return view('employees.edit', ['employee' => $my_employee, 'areas' => $areas, 'positions' => $positions]);

    }

    public function update(StoreEmployee $request){
        $id = $request->id;

        $my_employee = Employee::find($id);
        if($my_employee == NULL){
        return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }

        $my_employee->employee_id = $request->employee_id;
        $my_employee->first_name = $request->first_name;
        $my_employee->last_name = $request->last_name;
        $my_employee->birthdate = $request->birthdate;
        $my_employee->address = $request->address;
        $my_employee->telephone = $request->telephone;
        $my_employee->salary = $request->salary;
        $my_employee->area_id = $request->area_id;
        $my_employee->position_id = $request->position_id;
        $my_employee->date_of_admission = $request->date_of_admission;
        $rs = $my_employee->save();

        if($rs){
            return redirect('/employees')->with('success', 'Empleado actualizado correctamente');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
    }

    public function show(Request $request) {
        $id = $request->id;

        $my_employee = Employee::find($id);
        return view('employees.show', ['employee' => $my_employee]);
    }

    public function destroy(Request $request) {
        header('Access-Control-Allow-Origin: *');
        $id = $request->id;

        $my_employee = Employee::find($id);
        $rs = $my_employee->delete();

        echo $rs;
    }
}
