<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePosition;
use App\Position;
use App\Employee;

class PositionsController extends Controller
{
    public function index() {
        $positions = Position::all();
        return view('positions.index', ['positions' => $positions]);
    }
    public function new() {
        return view('positions.new');
    }

    public function create(StorePosition $request) {

        $my_position = new Position();
        $my_position->name = $request->name;
        $my_position->description = $request->description;
        $rs = $my_position->save();

        if($rs){
            return redirect('/positions/new')->with('success', 'Cargo creado correctamente');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        

    }
    public function edit(Request $request) {
        $id = $request->id;

        $my_position = Position::find($id);

        if($my_position == NULL){
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }

        return view('positions.edit', ['position' => $my_position]);

    }

    public function update(StorePosition $request){
        $id = $request->id;

        $my_position = Position::find($id);
        if($my_position == NULL){
        return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }

        $my_position->name = $request->name;
        $my_position->description = $request->description;

        $rs = $my_position->save();

        if($rs){
            return redirect('/positions')->with('success', 'Cargo actualizado correctamente');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
    }

    public function show(Request $request) {
        $id = $request->id;

        $my_position = Position::find($id);
        return view('positions.show', ['position' => $my_position]);
    }

    public function destroy(Request $request) {
        header('Access-Control-Allow-Origin: *');
        $id = $request->id;

        $my_position = Position::find($id);
        if($my_position == NULL){
            echo "100";
            return;
        }
        if(Employee::where('position_id', $my_position->id)->count() > 0){
            echo "101";
            return;
        }
        $rs = $my_position->delete();

        echo $rs;
    }


}
