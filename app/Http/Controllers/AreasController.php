<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreArea;
use App\Area;
use App\Employee;

class AreasController extends Controller
{
    public function index() {
        $areas = Area::all();
        return view('areas.index', ['areas' => $areas]);
    }
    public function new() {
        return view('areas.new');
    }

    public function create(StoreArea $request) {

        $my_area = new Area();
        $my_area->name = $request->name;
        $my_area->description = $request->description;
        $rs = $my_area->save();

        if($rs){
            return redirect('/areas/new')->with('success', 'Área creada ');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        
    }

    public function edit(Request $request) {
        $id = $request->id;

        $my_area = Area::find($id);

        if($my_area == NULL){
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
        
        return view('areas.edit', ['area' => $my_area]);

    }

    public function update(StoreArea $request){
        $id = $request->id;

        $my_area = Area::find($id);
        if($my_area == NULL){
        return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }

        $my_area->name = $request->name;
        $my_area->description = $request->description;

        $rs = $my_area->save();

        if($rs){
            return redirect('/areas')->with('success', 'Área actualizada');
        }else{
            return back()->with('notice', 'Ha ocurrido un error, verifique sus datos');
        }
    }

    public function show(Request $request) {
        $id = $request->id;

        $my_area = Area::find($id);
        return view('areas.show', ['area' => $my_area]);
    }

    public function destroy(Request $request) {
        header('Access-Control-Allow-Origin: *');
        $id = $request->id;

        $my_area = Area::find($id);
        if($my_area == NULL){
            echo "100";
            return;
        }
        if(Employee::where('area_id', $my_area->id)->count() > 0){
            echo "101";
            return;
        }
        $rs = $my_area->delete();

        echo $rs;
    }
}
