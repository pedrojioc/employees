<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required|max:45',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:100',
            'birthdate' => 'required|max:100',
            'address' => 'required|max:100',
            'telephone' => 'required|max:50',
            'salary' => 'required',
            'area_id' => 'required',
            'position_id' => 'required',
            'date_of_admission' => 'required|max:100'
            
        ];
    }
}
