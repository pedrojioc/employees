<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function area(){
        return $this->belongsTo('App\Area');
    }
    public function position(){
        return $this->belongsTo('App\Position');
    }
}
