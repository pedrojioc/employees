<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

Route::get('/areas', 'AreasController@index');
Route::get('/areas/new', 'AreasController@new');
Route::post('/areas/create', 'AreasController@create')->name('area.create');
Route::get('/areas/edit/{id}', 'AreasController@edit');
Route::post('/areas/update', 'AreasController@update')->name('area.update');
Route::get('/areas/show/{id}', 'AreasController@show');
Route::get('/areas/delete/{id}', 'AreasController@destroy');


Route::get('/positions', 'PositionsController@index');
Route::get('/positions/new', 'PositionsController@new');
Route::post('/positions/create', 'PositionsController@create')->name('position.create');
Route::get('/positions/edit/{id}', 'PositionsController@edit');
Route::post('/positions/update', 'PositionsController@update')->name('position.update');
Route::get('/positions/show/{id}', 'PositionsController@show');
Route::get('/positions/delete/{id}', 'PositionsController@destroy');


Route::get('/employees', 'EmployeesController@index');
Route::get('/employees/new', 'EmployeesController@new');
Route::post('/employees/create', 'EmployeesController@create')->name('employee.create');
Route::get('/employees/edit/{id}', 'EmployeesController@edit');
Route::post('/employees/update', 'EmployeesController@update')->name('employee.update');
Route::get('/employees/show/{id}', 'EmployeesController@show');
Route::get('/employees/delete/{id}', 'EmployeesController@destroy');